/* Functions for AJAX Calls */

/*var update_GDP_bar = function(year,month) {
	$.ajax({
		url: 'http://gh2015.scriptforge.org:5000/api/gdp/'+year+'/'+month,
		method: 'get',
		dataType: 'JSON',
//		jsonpCallback: 'callback',
		success: function(data) {
			var sumTotal = 0;
			var stateGDP = {WA:0,NT:0,SA:0,QLD:0,NSW:0,ACT:0,VIC:0,TAS:0}
			gdp = data;
			//console.log (gdp);
			for (var state in gdp){
				sumTotal += gdp[state];
				
			};
			for (var state in gdp){
				stateGDP[state]=((gdp[state]/sumTotal)*100).toFixed(2);
			}
			$("#pb-WA").css("width",stateGDP.WA+'%');
			$("#pb-NT").css("width",stateGDP.NT+'%');
			$("#pb-SA").css("width",stateGDP.SA+'%');
			$("#pb-QLD").css("width",stateGDP.QLD+'%');
			$("#pb-NSW").css("width",stateGDP.NSW+'%');
			$("#pb-ACT").css("width",stateGDP.ACT+'%');
			$("#pb-VIC").css("width",stateGDP.VIC+'%');
			$("#pb-TAS").css("width",stateGDP.TAS+'%');
			//console.log (gdp);
			//console.log (stateGDP.WA);
			//console.log (sumTotal);
		}
	});
};*/

function update_GDP_bar(timer_count)
{
	var sumTotal = 0;
	//var stateGDP = {WA:0,NT:0,SA:0,QLD:0,NSW:0,ACT:0,VIC:0,TAS:0}
	//gdp = data;
	//console.log (gdp);


	/*for (var state in gdp){
		sumTotal += gdp[state];
		
	};
	for (var state in gdp){
		stateGDP[state]=((gdp[state]/sumTotal)*100).toFixed(2);
	}
	*/

	sumTotal = gdpData['gdp'][timer_count]['gdp_aus'];
	wa_per = ((gdpData['gdp'][timer_count]['gsp_wa']/sumTotal)*100).toFixed(2) - 0.01;
	nt_per = ((gdpData['gdp'][timer_count]['gsp_nt']/sumTotal)*100).toFixed(2) - 0.01;
	sa_per = ((gdpData['gdp'][timer_count]['gsp_sa']/sumTotal)*100).toFixed(2) - 0.01;
	qld_per = ((gdpData['gdp'][timer_count]['gsp_qld']/sumTotal)*100).toFixed(2) - 0.01;
	nsw_per = ((gdpData['gdp'][timer_count]['gsp_nsw']/sumTotal)*100).toFixed(2) - 0.01;
	act_per = ((gdpData['gdp'][timer_count]['gsp_act']/sumTotal)*100).toFixed(2) - 0.01;
	vic_per = ((gdpData['gdp'][timer_count]['gsp_vic']/sumTotal)*100).toFixed(2) - 0.01;
	tas_per = ((gdpData['gdp'][timer_count]['gsp_tas']/sumTotal)*100).toFixed(2) - 0.01;

	$("#pb-WA").css("width",wa_per+'%');
	$("#pb-NT").css("width",nt_per+'%');
	$("#pb-SA").css("width",sa_per+'%');
	$("#pb-QLD").css("width",qld_per+'%');
	$("#pb-NSW").css("width",nsw_per+'%');
	$("#pb-ACT").css("width",act_per+'%');
	$("#pb-VIC").css("width",vic_per+'%');
	$("#pb-TAS").css("width",tas_per+'%');
}

$(document).ready(function(){
	

	$("#Load_GDP_Data").click(function(){
		//console.log ("This button works!");
		update_GDP_bar('2014','6');
	});
	$("#Stop_Timer").click(function(){
		console.log("Stop Time!");
		
	});
	
});
