var geojson;
var initialBounds = [-28.8, 134];
var initialZoom = 5;
var mapWidth = 1200;
var mapHeight = 900;
var midPoint = L.point(mapWidth/2, mapHeight/2);
var lineColour = '#E0E0E0';
var lineColourHover = '#FFFFF';
stateCodeEnum = {
    NSW : 1,
    VIC : 2,
    QLD : 3,
    SA : 4,
    WA : 5,
    TAS : 6,
    NT : 7,
    ACT : 8,
};
var currentStateTarget = -1;
var currentYear = 1990;
var gdpData;

function initialiseGDPData()
{
    var jsonData = $.getJSON('/api/gdp2', function()
    {
        gdpData = jsonData['responseJSON'];
        geojson = L.geoJson(statesPointData,
        {
            style: style,
            onEachFeature: onEachFeature
        }).addTo(map);
    });
}

function getSelectedStateCode()
{
    return parseInt(currentStateTarget.feature.properties.STATE_CODE);
}

function getStateName(stateCode, shortName)
{
    switch(stateCode)
    {
        case stateCodeEnum.NSW:
            return shortName ? 'NSW' : 'New South Wales';
            break;
        case stateCodeEnum.VIC:
            return shortName ? 'VIC' : 'Victoria';
            break;
        case stateCodeEnum.QLD:
            return shortName ? 'QLD' : 'Queensland';
            break;
        case stateCodeEnum.SA:
            return shortName ? 'SA' : 'South Australia';
            break;
        case stateCodeEnum.WA:
            return shortName ? 'WA' : 'Western Australia';
            break;
        case stateCodeEnum.TAS:
            return shortName ? 'TAS' : 'Tasmania';
            break;
        case stateCodeEnum.NT:
            return shortName ? 'NT' : 'Northern Territory';
            break;
        case stateCodeEnum.ACT:
            return shortName ? 'ACT' : 'Australian Capital Territory';
            break;
        default:
            return 'getSelectedStateName ERROR';
            break;
    }
}

function getStateColour(stateCode)
{
    switch(stateCode)
    {
        case stateCodeEnum.NSW:
            return '#31b0d5';
            break;
        case stateCodeEnum.VIC:
            return 'blue';
            break;
        case stateCodeEnum.QLD:
            return 'maroon';
            break;
        case stateCodeEnum.SA:
            return '#d9534f';
            break;
        case stateCodeEnum.WA:
            return 'yellow';
            break;
        case stateCodeEnum.TAS:
            return '#449d44';
            break;
        case stateCodeEnum.NT:
            return '#FFA500';
            break;
        case stateCodeEnum.ACT:
            return 'black';
            break;
        default:
            return '#FFF';
            break;
    }
}

function style(feature)
{
    return {
        fillColor: getStateColour(parseInt(feature.properties.STATE_CODE)),
        weight: 2,
        color: lineColour,
        dashArray: '3',
        opacity: 1,
        fillOpacity: 0.3
    };
}

function highlightFeature(e)
{
    var layer = e.target;
    layer.setStyle(
    {
        weight: 2,
        color: lineColourHover,
        dashArray: '',
        opacity: 1,
        fillOpacity: 0.6
    });
    if (!L.Browser.ie && !L.Browser.opera)
    {
        layer.bringToFront();
    }
    currentStateTarget = e.target;
}

function resetHighlight(e)
{
    geojson.resetStyle(e.target);
}

function zoomToFeature(e)
{
    currentStateTarget = e.target;
    $('#modalStateInformation').modal('show');
    $('#modalStateInformation').find('.modal-title').text(getStateName(getSelectedStateCode(), false));
}

function onEachFeature(feature, layer)
{
    var iconContents = '<h4>' + feature.properties.STATE_NAME + '</h4>GSP: $' + gdpData['gdp'][timer_count]['gsp_'+getStateName(parseInt(feature.properties.STATE_CODE),true).toLowerCase()] + ' (millions)';
    var pos = {
        1: map.containerPointToLatLng(map.latLngToContainerPoint(layer.getBounds().getCenter()).add(L.point(100,-50))), //NSW
        2: map.containerPointToLatLng(map.latLngToContainerPoint(layer.getBounds().getCenter()).add(L.point(0,50))), //VIC
        3: map.containerPointToLatLng(map.latLngToContainerPoint(layer.getBounds().getCenter())), //QLD
        4: map.containerPointToLatLng(map.latLngToContainerPoint(layer.getBounds().getCenter())), //SA
        5: map.containerPointToLatLng(map.latLngToContainerPoint(layer.getBounds().getCenter())), //WA
        6: map.containerPointToLatLng(map.latLngToContainerPoint(layer.getBounds().getCenter())), //TAS
        7: map.containerPointToLatLng(map.latLngToContainerPoint(layer.getBounds().getCenter())), //NT
        8: map.containerPointToLatLng(map.latLngToContainerPoint(layer.getBounds().getCenter()).add(L.point(0,-25)))  //ACT
    };
    var stateIcon = L.divIcon({
        className: 'state-icon-'+getStateName(parseInt(feature.properties.STATE_CODE),true).toLowerCase(),
        html: iconContents,
        iconSize: L.point(250, 50)
    });
    L.marker(pos[feature.properties.STATE_CODE], {icon: stateIcon}).addTo(map);
    layer.on(
    {
        mouseover: highlightFeature,
        mouseout: resetHighlight,
    });
}

function updateGSPs(timer_count)
{
    var iconContents = '<h4>Queensland</h4>GSP: $' + gdpData['gdp'][timer_count]['gsp_qld'] + ' (millions)';
    $('.state-icon-qld').html(iconContents);
    var iconContents = '<h4>Northern Territory</h4>GSP: $' + gdpData['gdp'][timer_count]['gsp_nt'] + ' (millions)';
    $('.state-icon-nt').html(iconContents);
    var iconContents = '<h4>South Australia</h4>GSP: $' + gdpData['gdp'][timer_count]['gsp_sa'] + ' (millions)';
    $('.state-icon-sa').html(iconContents);
    var iconContents = '<h4>New South Wales</h4>GSP: $' + gdpData['gdp'][timer_count]['gsp_nsw'] + ' (millions)';
    $('.state-icon-nsw').html(iconContents);
    var iconContents = '<h4>Western Australia</h4>GSP: $' + gdpData['gdp'][timer_count]['gsp_wa'] + ' (millions)';
    $('.state-icon-wa').html(iconContents);
    var iconContents = '<h4>Victoria</h4>GSP: $' + gdpData['gdp'][timer_count]['gsp_vic'] + ' (millions)';
    $('.state-icon-vic').html(iconContents);
    var iconContents = '<h4>Australian Capital Territory</h4>GSP: $' + gdpData['gdp'][timer_count]['gsp_act'] + ' (millions)';
    $('.state-icon-act').html(iconContents);
    var iconContents = '<h4>Tasmania</h4>GSP: $' + gdpData['gdp'][timer_count]['gsp_tas'] + ' (millions)';
    $('.state-icon-tas').html(iconContents);
}

$(document).ready()
{
    var map = L.map('map', {zoomControl:false}).setView(initialBounds, initialZoom);
    map.dragging.disable();
    map.touchZoom.disable();
    map.doubleClickZoom.disable();
    map.scrollWheelZoom.disable();
    if(map.tap) map.tap.disable();

    var mapboxAttribution = 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>';
    var mapboxAccessToken = 'pk.eyJ1IjoicG0wIiwiYSI6IjNlY2E0ZDllMzM4MmJjYWI5ZmEyNWFhNzdiN2VlNDhiIn0.-SVhg6J9yy5wggdJ7id5_Q';
    var plainThemeUrl = 'https://api.tiles.mapbox.com/v4/pm0.fe002627/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoicG0wIiwiYSI6IjNlY2E0ZDllMzM4MmJjYWI5ZmEyNWFhNzdiN2VlNDhiIn0.-SVhg6J9yy5wggdJ7id5_Q';
    var darkThemeUrl = 'https://api.tiles.mapbox.com/v4/pm0.mkf0fg4h/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoicG0wIiwiYSI6IjNlY2E0ZDllMzM4MmJjYWI5ZmEyNWFhNzdiN2VlNDhiIn0.-SVhg6J9yy5wggdJ7id5_Q';
    var plainTheme = L.tileLayer(plainThemeUrl, {id: 'pm0.fe002627', attribution: mapboxAttribution, accessToken: mapboxAccessToken});
    var darkTheme = L.tileLayer(darkThemeUrl, {id: 'pm0.mkf0fg4h', attribution: mapboxAttribution, accessToken: mapboxAccessToken});
    plainTheme.addTo(map);

    initialiseGDPData();
}
